# Cachet Docker image
#
# Copyright (C) 2020, 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PHP_IMAGE_TAG=7.4.11-fpm-alpine3.12
FROM php:${PHP_IMAGE_TAG}
MAINTAINER LSF operations team <ops@libre.space>

ARG CACHET_VERSION=cfd173cf122d925b70d5133a37528db6120bea67
ARG PHP_APCU_VERSION=5.1.19
ARG PHP_REDIS_VERSION=5.3.1
ARG PHP_MCRYPT_VERSION=1.0.3
ARG COMPOSER_VERSION=2.4.2

WORKDIR /tmp

# Use production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# Install system packages
RUN apk add --no-cache \
	rsync \
	gmp \
	icu \
	libjpeg \
	libpng \
	libwebp \
	libxml2 \
	libzip \
	libmcrypt \
	zlib \
	postgresql-client \
	mysql-client

# Get Cachet source code
RUN wget https://github.com/cachethq/Cachet/archive/${CACHET_VERSION}.tar.gz \
	&& mkdir -p /usr/local/share/cachet \
	&& tar -C /usr/local/share/cachet -xzf ${CACHET_VERSION}.tar.gz --strip-components=1 \
	&& rm ${CACHET_VERSION}.tar.gz

# Install PHP extensions
RUN apk add --no-cache --virtual .build-deps \
		${PHPIZE_DEPS} \
		gmp-dev \
		icu-dev \
		jpeg-dev \
		libpng-dev \
		libwebp-dev \
		libxml2-dev \
		libzip-dev \
		libmcrypt-dev \
		postgresql-dev \
		zlib-dev \
		postgresql-client \
		mysql-client \
	&& pecl channel-update pecl.php.net \
	&& pecl install \
		APCu-${PHP_APCU_VERSION} \
		redis-${PHP_REDIS_VERSION} \
		mcrypt-${PHP_MCRYPT_VERSION} < /dev/null \
	&& docker-php-ext-configure \
		gd \
			--with-jpeg \
			--with-webp \
	&& docker-php-ext-install -j "$(nproc)" \
		bcmath \
		gd \
		intl \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		soap \
		zip \
	&& docker-php-ext-enable apcu redis mcrypt \
	&& pecl clear-cache \
	&& apk del --no-network .build-deps

# Install Cachet and dependencies
WORKDIR /usr/local/share/cachet
RUN curl -sS https://getcomposer.org/installer | php -- --version ${COMPOSER_VERSION} --install-dir="/usr/local/bin" --filename="composer" \
	&& composer -n config allow-plugins.kylekatarnls/update-helper true \
	&& composer install -n -o --no-dev \
	&& rm /usr/local/bin/composer

WORKDIR /var/www/html
VOLUME /var/www/html

COPY cachet /usr/local/bin/cachet

CMD ["cachet"]
